import Vue from 'vue';
import { mount } from 'avoriaz';
import ForecastCard from '@/components/ForecastCard';

const datapoint = require('../mock/card-datapoint');

describe('ForecastCard.vue', () => {
  it('should have dom elements', () => {
    const wrapper = mount(ForecastCard, {
      propsData: { datapoint },
    });
    expect(wrapper.find('.card').length).to.equal(1);
    expect(wrapper.find('.weather-icon').length).to.equal(1);
    expect(wrapper.find('.description').length).to.equal(1);
    expect(wrapper.find('.humidity').length).to.equal(1);
    expect(wrapper.find('.wind').length).to.equal(1);
    expect(wrapper.find('.time').length).to.equal(1);
  });

  it('should poplate correct data', (done) => {
    const wrapper = mount(ForecastCard, {
      propsData: { datapoint },
    });

    Vue.nextTick(() => {
      expect(wrapper.find('.description')[0].text()).to.equal(datapoint.weather[0].description);
      expect(wrapper.find('.weather-icon')[0].hasAttribute('src', `http://openweathermap.org/img/w/${datapoint.weather[0].icon}.png`)).to.equal(true);
      expect(wrapper.find('.time')[0].text()).to.equal('11:30 AM');
      expect(wrapper.find('.temperature')[0].text()).to.contain(datapoint.main.temp);
      expect(wrapper.find('.humidity .value')[0].text()).to.contain(datapoint.main.humidity);
      expect(wrapper.find('.wind .value')[0].text()).to.contain(datapoint.wind.speed);
      done();
    });
  });
});
