import processWeatherData from '@/utils';

const rawResponse = require('../mock/raw-response');

describe('processWeatherData', () => {
  it('should return object with city and data keys', () => {
    const resp = processWeatherData(rawResponse);
    expect(resp.data).to.be.instanceof(Array);
    expect(resp.city).to.have.all.keys(['id', 'name', 'coord', 'country']);
  });

  it('should return object with correct city', () => {
    const resp = processWeatherData(rawResponse);
    expect(resp.city).to.equal(rawResponse.city);
  });

  it('should return object with correct data', () => {
    const resp = processWeatherData(rawResponse);
    expect(resp.data.length).to.equal(5);
    expect(resp.data[0]).to.have.all.keys(['list', 'dateKey', 'title']);
  });
});
