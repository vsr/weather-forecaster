import Vue from 'vue';
import { mount } from 'avoriaz';
import DayForecast from '@/components/DayForecast';

const dayWeatherData = require('../mock/forecast');

describe('DayForecast.vue', () => {
  it('should have dom elements', (done) => {
    const wrapper = mount(DayForecast, {
      propsData: { dayWeatherData: dayWeatherData.data[0] },
      attachToDocument: true,
    });
    const wrapper2 = mount(DayForecast, {
      propsData: { dayWeatherData: dayWeatherData.data[1] },
      attachToDocument: true,
    });
    Vue.nextTick(() => {
      expect(wrapper.find('.day-title').length).to.equal(1);
      expect(wrapper.find('.data-point').length).to.equal(5);
      expect(wrapper2.find('.day-title').length).to.equal(1);
      expect(wrapper2.find('.data-point').length).to.equal(8);
      done();
    });
  });

  it('should populate elements', (done) => {
    const wrapper = mount(DayForecast, {
      propsData: { dayWeatherData: dayWeatherData.data[0] },
      attachToDocument: true,
    });

    const wrapper2 = mount(DayForecast, {
      propsData: { dayWeatherData: dayWeatherData.data[1] },
      attachToDocument: true,
    });
    Vue.nextTick(() => {
      expect(wrapper.find('.day-title')[0].text()).to.equal('Thursday, Apr 27');
      expect(wrapper2.find('.day-title')[0].text()).to.equal('Friday, Apr 28');
      done();
    });
  });
});
