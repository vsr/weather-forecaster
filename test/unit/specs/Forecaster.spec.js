/* eslint-disable no-unused-expressions */
import Vue from 'vue';
import { mount } from 'avoriaz';
import Forecaster from '@/components/Forecaster';

const dayWeatherData = require('../mock/forecast');


describe('Forecaster.vue', () => {
  it('should have methods defined', () => {
    expect(typeof Forecaster.methods.locationChange).to.equal('function');
    expect(typeof Forecaster.methods.getWeather).to.equal('function');
  });

  it('should have default data defined', () => {
    const wrapper = mount(Forecaster, {
      attachToDocument: true,
    });
    expect(wrapper.data().location).to.equal('Bangalore');
    expect(wrapper.data().weatherData).to.be.empty;
  });

  it('should have initial dom elements', (done) => {
    const wrapper = mount(Forecaster, {
      attachToDocument: true,
    });
    Vue.nextTick(() => {
      expect(wrapper.find('.location-form').length).to.equal(1);
      done();
    });
  });

  it('should have dom elements with data loaded', (done) => {
    const wrapper = mount(Forecaster, {
      attachToDocument: true,
    });
    wrapper.data().weatherData = dayWeatherData;
    Vue.nextTick(() => {
      expect(wrapper.find('.location-form').length).to.equal(1);
      expect(wrapper.find('.location-title').length).to.equal(1);
      expect(wrapper.find('.weather-data-container').length).to.equal(1);
      expect(wrapper.find('.day-forecast').length).to.equal(5);
      done();
    });
  });

  it('should call getWeather on mount', () => {
    const getWeatherSpy = sinon.spy(Forecaster.methods, 'getWeather');
    const wrapper = mount(Forecaster, {
      attachToDocument: true,
    });
    expect(wrapper.data().location).to.equal('Bangalore');
    expect(getWeatherSpy.called).to.equal(true);
    Forecaster.methods.getWeather.restore();
  });

  it('should call locationChange on submit', (done) => {
    const locationChangeSpy = sinon.spy(Forecaster.methods, 'locationChange');
    const wrapper = mount(Forecaster, {
      attachToDocument: true,
    });
    wrapper.data().location = 'London, UK';
    wrapper.find('.location-form')[0].simulate('submit');
    Vue.nextTick(() => {
      expect(locationChangeSpy.calledOnce).to.equal(true);
      done();
    });
  });

  it('should call getWeather on submit', (done) => {
    const getWeatherSpy = sinon.spy(Forecaster.methods, 'getWeather');
    const wrapper = mount(Forecaster, {
      attachToDocument: true,
    });
    wrapper.data().location = 'London, UK';
    wrapper.find('.location-form')[0].simulate('submit');
    Vue.nextTick(() => {
      expect(getWeatherSpy.calledTwice).to.equal(true);
      done();
    });
  });
});
