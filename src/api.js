import 'whatwg-fetch';
import { API_URL, API_KEY } from './config';
import processWeatherData from './utils';


/*
* Fetches weather data for given city name
* Params (cityName : String)
* Returns Promise
*/
function getWeatherByName(cityName) {
  const prom = new Promise((resolve, reject) => {
    const url = `${API_URL}?q=${cityName}&units=metric&APPID=${API_KEY}`;
    fetch(url).then(response => response.json()).then((json) => {
      resolve(processWeatherData(json));
    }).catch((err) => {
      reject(err);
    });
  });

  return prom;
}


export default getWeatherByName;
