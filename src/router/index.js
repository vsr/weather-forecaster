import Vue from 'vue';
import Router from 'vue-router';
import Forecaster from '@/components/Forecaster';

Vue.use(Router);
// Router for main application- imported by main.js to link with App.vue
export default new Router({
  routes: [
    {
      path: '/',
      name: 'Forecaster',
      component: Forecaster,
    },
  ],
});
