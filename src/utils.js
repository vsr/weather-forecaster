import moment from 'moment';

function processWeatherData(data) {
  // Weather info which resolves the promise
  const weatherData = { city: data.city, data: [] };
  // Sort the data points by date- just in case they're out of order
  data.list.sort((a, b) => (a.dt - b.dt));

  let currentDate;
  let weatherDay;

  // loop through the API response to group data by date for UI representation
  data.list.forEach((d) => {
    const dateObj = moment(d.dt * 1000);
    const dateKey = dateObj.format('YYYY-MM-DD');

    if (currentDate !== dateKey) {
      if (weatherDay && weatherDay.list.length > 0) {
        weatherData.data.push(weatherDay);
      }
      currentDate = dateKey;
      weatherDay = { list: [],
        dateKey,
        title: dateObj.format('dddd, MMM DD'),
      };
    }

    weatherDay.list.push(d);
  });
  return weatherData;
}

export default processWeatherData;
