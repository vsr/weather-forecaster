# weather-forecast

> A weather forecasting app

Running live at [http://weatherforecast-165304.appspot.com/](http://weatherforecast-165304.appspot.com/)

(ps: http, not https)

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# run tests
npm test
```

## About tech

Forecaster app uses [Vue.js](https://vuejs.org/) 2.0  and is based on the [webpack template](https://github.com/vuejs-templates/webpack).


## todo

* Write more unit tests
* Use Vuex store for storing data
* Reveal more information on interaction with card
* Feature for changing location
* Remove API key from config file
* Layout could be better, maybe aggregation and drilldown? Need to experiment/research.

